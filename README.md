# Sample GitLab Project

This sample repo shows how I would develop a project.

# Install instructions
To install all required dependencies, run

    - sudo sh install.sh

# Build instractions
    - cmake .
    - make all

# Run program
    - ./filmsAndSeries

# TO-DO list
-   Update infromation of shows in the DB
-   Delete show in the DB
-   Add show to the DB
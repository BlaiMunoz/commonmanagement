#include <gtest/gtest.h>
#include <gmock/gmock.h>

int main(int argc,
         char * argv[], char * envp[])
{
    testing::InitGoogleMock(&argc, argv);
    testing::InitGoogleTest(&argc, argv);

    int ret_val = RUN_ALL_TESTS();

    return ret_val;
}
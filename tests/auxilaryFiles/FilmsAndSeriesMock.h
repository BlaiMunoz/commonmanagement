#ifndef _FILMS_AND_SERIES_MOCK_H_
#define _FILMS_AND_SERIES_MOCK_H_

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "core/FilmsAndSeriesAbs.h"

class FilmsAndSeriesMock: public FilmsAndSeriesAbs
{
	public:
		virtual ~FilmsAndSeriesMock() {};
		MOCK_METHOD(void, showAll, (), (override));
		MOCK_METHOD(void, showSeries, (), (override));
		MOCK_METHOD(void, showFilms, (), (override));
		MOCK_METHOD(void, getInfo, (const std::string &showName), (override));
};

using FilmsAndSeriesMockPtr = std::shared_ptr<FilmsAndSeriesMock>;

#endif /* _FILMS_AND_SERIES_MOCK_H_ */

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <fstream>
#include <chrono>

#include "console/Console.h"
#include "auxilaryFiles/FilmsAndSeriesMock.h"

TEST(test_console, consoleFirstStatusIsIdle)
{
    std::ifstream stopConsoleFile;
    stopConsoleFile.open("tests/auxilaryFiles/stopConsole", std::ifstream::in);
    FilmsAndSeriesMockPtr dataBase = std::make_shared<FilmsAndSeriesMock>();
    ConsolePtr console = std::make_shared<Console>(dataBase, stopConsoleFile);


    EXPECT_TRUE(console->getStatus() ==  false);
}

TEST(test_console, theStatusOfTheConsoleCanGoFromIdleToRunningAndBack)
{
    FilmsAndSeriesMockPtr dataBase = std::make_shared<FilmsAndSeriesMock>();
    ConsolePtr console = std::make_shared<Console>(dataBase);

    console->start();

    EXPECT_TRUE(console->getStatus());
    console->stop();
    EXPECT_FALSE(console->getStatus());
}

TEST(test_console, UserInputChangesConsoleStateFromIdleToRunningAndBack)
{
    std::ifstream stopConsoleFile;
    stopConsoleFile.open("tests/auxilaryFiles/stopConsole", std::ifstream::in);
    FilmsAndSeriesMockPtr dataBase = std::make_shared<FilmsAndSeriesMock>();
    ConsolePtr console = std::make_shared<Console>(dataBase, stopConsoleFile);

    console->start();

    sleep(1);

    EXPECT_FALSE(console->getStatus());
}

TEST(test_console, whenUserInputsShallAllTheCorrespondingFunctionIsCalled)
{
    std::ifstream stopConsoleFile;
    stopConsoleFile.open("tests/auxilaryFiles/showAll", std::ifstream::in);
    FilmsAndSeriesMockPtr dataBase = std::make_shared<FilmsAndSeriesMock>();
    ConsolePtr console = std::make_shared<Console>(dataBase, stopConsoleFile);
    EXPECT_CALL((*dataBase), showAll());

    console->start();

    sleep(1);
}

TEST(test_console, whenUserInputsShallSeriesTheCorrespondingFunctionIsCalled)
{
    std::ifstream stopConsoleFile;
    stopConsoleFile.open("tests/auxilaryFiles/showSeries", std::ifstream::in);
    FilmsAndSeriesMockPtr dataBase = std::make_shared<FilmsAndSeriesMock>();
    ConsolePtr console = std::make_shared<Console>(dataBase, stopConsoleFile);
    EXPECT_CALL((*dataBase), showSeries());

    console->start();

    sleep(1);
}

TEST(test_console, whenUserInputsShallFilmsTheCorrespondingFunctionIsCalled)
{
    std::ifstream stopConsoleFile;
    stopConsoleFile.open("tests/auxilaryFiles/showFilms", std::ifstream::in);
    FilmsAndSeriesMockPtr dataBase = std::make_shared<FilmsAndSeriesMock>();
    ConsolePtr console = std::make_shared<Console>(dataBase, stopConsoleFile);
    EXPECT_CALL((*dataBase), showFilms());

    console->start();

    sleep(1);
}
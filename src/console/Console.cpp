#include "console/Console.h"

static const int MAX_NAME_LEN = 60;

Console::Console(FilmsAndSeriesAbsPtr FilmsAndSeries, std::istream& input):
mFilmsAndSeries(FilmsAndSeries),
mConsoleThreadHandler(&Console::consoleThread, this), 
mInput(input)         
{
    help();
}

Console::~Console()
{
	mRunning = false;
	mDestroy = true;
    if (mConsoleThreadHandler.joinable())
    {
        mConsoleThreadHandler.join();
    }    
}

void Console::start()
{
    mRunning = true;
}

void Console::stop()
{
    mRunning = false;
}

bool Console::getStatus()
{
    return mRunning;
}

void Console::help()
{
    std::cout << "\r\nAvailable commands:\r\n";
    std::cout << "showAll    --------> Show all the data in the DB\r\n";
    std::cout << "showFilms  --------> Show all the films in the DB\r\n";
    std::cout << "showSeries --------> Show all the series in the DB\r\n";
    std::cout << "get        --------> Get more info about an specific show\r\n";
    std::cout << "stop       --------> Stop the Console DB\r\n";
    std::cout << "start      --------> Start the console\r\n";
}

void Console::consoleThread()
{   
    while(!mDestroy)
	{
        readUserInput();
		if(mRunning)
		{          
            if(mUserInput.compare("stop") == 0)
            {
                stop();
            }
            else if(mUserInput.compare("showAll") == 0)
            {
                mFilmsAndSeries->showAll();
            }else if(mUserInput.compare("showSeries") == 0)
            {
                mFilmsAndSeries->showSeries();
            }else if(mUserInput.compare("showFilms") == 0)
            {
                mFilmsAndSeries->showFilms();
            }else if(mUserInput.compare("get") == 0)
            {
                std::cout << "Type the name of the show:\r\n";
                std::string showName;
                std::cin >> showName;
                mFilmsAndSeries->getInfo(showName);
            }
            else
            {
                std::cout << "Error, command [" << mUserInput <<" ] not identified\r\n";
            }
            help();
		}
        else
        {
            if(mUserInput.compare("start") == 0)
            {
                start();
            }
        }	
        std::this_thread::sleep_for(std::chrono::milliseconds(250));
	}
}

void Console::readUserInput()
{
    mInput >> mUserInput;
}
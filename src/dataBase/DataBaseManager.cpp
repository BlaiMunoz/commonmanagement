#include "dataBase/DataBaseManager.h"
#include <iostream>

DataBaseManager::DataBaseManager()
{
    
}

DataBaseManager::~DataBaseManager()
{
    if (mDBFile.is_open())
    {
        mDBFile.close();
    }
}

bool DataBaseManager::loadDataBase(Json::Value &dataBase)
{
    mDBFile.open ("filmsAndSeries.json", std::ifstream::in);
    if(!mDBFile.good())
    {
        std::cout << "Error opening file\r\n";
        return false;
    }
    mDBFile >> dataBase;
    mDBFile.close();
    return true;
}
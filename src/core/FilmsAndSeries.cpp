#include "core/FilmsAndSeries.h"
#include <iostream>


FilmsAndSeries::FilmsAndSeries(DataBaseManagerAbsPtr databaseManager, RadDataFromDataBaseAbsPtr managerFilms):
mDatabaseManager(databaseManager),
mManagerFilms(managerFilms)
{
    if( mDatabaseManager->loadDataBase(mDB) == false )
    {
        std::cout << "Unable to load database\r\n";
    }
}

FilmsAndSeries::~FilmsAndSeries()
{

}

void FilmsAndSeries::showAll()
{
    std::cout << "Film names in the DB\r\n";
    mManagerFilms->showFilmNames(mDB, "film");
    std::cout << "Series names in the DB\r\n";
    mManagerFilms->showFilmNames(mDB, "series");
}

void FilmsAndSeries::showFilms()
{
    std::cout << "Film names in the DB\r\n";
    mManagerFilms->showFilmNames(mDB, "film");
}

void FilmsAndSeries::showSeries()
{
    std::cout << "Series names in the DB\r\n";
    mManagerFilms->showFilmNames(mDB, "series");
}

void FilmsAndSeries::getInfo(const std::string &showName)
{
    std::cout << "You want info about: " << showName << "\r\n";
    mManagerFilms->showInfo(mDB[showName]);
}
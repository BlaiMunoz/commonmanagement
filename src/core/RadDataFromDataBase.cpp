#include "core/RadDataFromDataBase.h"
#include <iostream>
#include <vector>

RadDataFromDataBase::RadDataFromDataBase()
{

}

RadDataFromDataBase::~RadDataFromDataBase()
{

}

void RadDataFromDataBase::showFilmNames(Json::Value &data, const std::string &showType)
{
    for(Json::Value::iterator it = data.begin(); it != data.end(); it++)
    {
        if(data[it.name()]["type"][0] == showType)
        {
            std::cout << "-----> " << it.name() << "\r\n";
        }        
    }
}

void RadDataFromDataBase::showInfo(Json::Value &data)
{
    for(Json::Value::iterator it = data.begin(); it != data.end(); it++)
    {
        std::string parameter = it.name();
        std::cout << "---> " << parameter << "\r\n";
        for(int numberOfParameters = 0; numberOfParameters < data[parameter].size(); numberOfParameters++)
        {
            std::cout << "------>" << data[parameter][numberOfParameters] << "\r\n";
        }
    }
}
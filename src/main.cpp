#include <iostream>
#include <signal.h>

#include "console/Console.h"
#include "core/FilmsAndSeriesAbs.h"
#include "core/FilmsAndSeries.h"
#include "core/RadDataFromDataBase.h"
#include "core/RadDataFromDataBaseAbs.h"
#include "dataBase/DataBaseManagerAbs.h"
#include "dataBase/DataBaseManager.h"

void intHandler(int dummy)
{
    std::cout << "User pressed Ctrl+C\r\n";
    exit(1);
}

int main()
{
     
    DataBaseManagerAbsPtr dataBaseManager = std::make_shared<DataBaseManager>();
    RadDataFromDataBaseAbsPtr managerFilms = std::make_shared<RadDataFromDataBase>();
    FilmsAndSeriesPtr dataBase = std::make_shared<FilmsAndSeries>(dataBaseManager, managerFilms);
    ConsolePtr console = std::make_shared<Console>(dataBase);

    console->start();


	signal(SIGINT, intHandler);

    while(1);
    
    return 0;
}

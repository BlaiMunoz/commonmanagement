cmake_minimum_required(VERSION 3.16.3)
project(Films&Series)

if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
  set(CMAKE_INSTALL_PREFIX "/out" CACHE PATH "..." FORCE)
endif()

set( EXECUTABLE "filmsAndSeries" )
# set( TEST_NAME "testFilmsAndSeries" )

set (SRC_CPP "src")
set (INC_CPP "include/")
# set (TEST_CPP "tests/")

set (CMAKE_CXX_STANDARD 11)

file(GLOB BIN_SOURCES   "${SRC_CPP}/*.cpp"
                        "${SRC_CPP}/console/*.cpp"
                        "${SRC_CPP}/dataBase/*.cpp"
                        "${SRC_CPP}/core/*.cpp" )

# find_package(GTest REQUIRED)

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
find_package(PkgConfig REQUIRED)
pkg_check_modules(JSONCPP jsoncpp)

include_directories(
                    # ${GTEST_INCLUDE_DIRS}
                    # ${TEST_CPP}                    
                    ${INC_CPP} ) 

# file(GLOB TEST_BIN_SOURCES  "${TEST_CPP}/*.cpp"
#                             "${SRC_CPP}/console/*.cpp"
#                             "${SRC_CPP}/dataBase/*.cpp"
#                             "${SRC_CPP}/core/*.cpp" )


add_executable( ${EXECUTABLE} ${BIN_SOURCES} )
# add_executable( ${TEST_NAME} ${TEST_BIN_SOURCES} )
# target_link_libraries(${TEST_NAME} PRIVATE
#                       /lib/libgtest.a
#                       /lib/libgtest_main.a
#                       /lib/libgmock.a
#                       pthread
#                       Threads::Threads
#                       ${JSONCPP_LIBRARIES}) 


target_link_libraries(${EXECUTABLE} PRIVATE
                      pthread
                      ${JSONCPP_LIBRARIES}) 
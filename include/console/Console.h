#ifndef _CONSOLE_H_
#define _CONSOLE_H_

#include <thread>
#include <string>
#include <iostream>

#include "core/FilmsAndSeriesAbs.h"

class Console
{
    public:
        Console(FilmsAndSeriesAbsPtr FilmsAndSeries, std::istream& input = std::cin);
        ~Console();

        bool getStatus();
        void start();
        void stop();
        void help();

    private:
        bool mDestroy{false};
		bool mRunning{false};
        FilmsAndSeriesAbsPtr mFilmsAndSeries;
        std::string mUserInput;

        std::thread mConsoleThreadHandler;
        std::istream& mInput;

        void consoleThread();
        void readUserInput();
};

using ConsolePtr = std::shared_ptr<Console>;

#endif /* _CONSOLE_H_ */
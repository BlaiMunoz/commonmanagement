#ifndef _READ_DATA_FROM_DATABASE_H
#define _READ_DATA_FROM_DATABASE_H

#include "core/RadDataFromDataBaseAbs.h"

class RadDataFromDataBase: public RadDataFromDataBaseAbs
{
	public:
        RadDataFromDataBase();
		virtual ~RadDataFromDataBase();
		void showFilmNames(Json::Value &data, const std::string &showType) override;
		void showInfo(Json::Value &data) override;		
};

#endif /* _READ_DATA_FROM_DATABASE_H */

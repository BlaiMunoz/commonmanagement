#ifndef _FILMS_AND_SERIES_H_
#define _FILMS_AND_SERIES_H_

#include "core/FilmsAndSeriesAbs.h"
#include "core/RadDataFromDataBaseAbs.h"
#include "dataBase/DataBaseManagerAbs.h"

class FilmsAndSeries: public FilmsAndSeriesAbs
{
	public:
        FilmsAndSeries(DataBaseManagerAbsPtr databaseManager, RadDataFromDataBaseAbsPtr managerFilms);
		virtual ~FilmsAndSeries();
		void showAll() override;
		void showSeries() override;
		void showFilms() override;
		void getInfo(const std::string &showName) override;

	private:
		DataBaseManagerAbsPtr mDatabaseManager;
		RadDataFromDataBaseAbsPtr mManagerFilms;
		Json::Value mDB;
};

using FilmsAndSeriesPtr =  std::shared_ptr<FilmsAndSeries>;

#endif /* _FILMS_AND_SERIES_H_ */

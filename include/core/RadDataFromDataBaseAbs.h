
#ifndef _READ_DATA_FROM_DATABASE_ABS_H_
#define _READ_DATA_FROM_DATABASE_ABS_H_

#include <memory>
#include <string>
#include "jsoncpp/json/json.h"

class RadDataFromDataBaseAbs
{
	public:
		virtual void showFilmNames(Json::Value &data, const std::string &showType) = 0;
		virtual void showInfo(Json::Value &data) = 0;
};

using RadDataFromDataBaseAbsPtr =  std::shared_ptr<RadDataFromDataBaseAbs>;

#endif /* _MANAGE_FILMS_ABS_H_ */
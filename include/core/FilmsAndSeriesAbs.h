
#ifndef _FILMS_AND_SERIES_ABS_H_
#define _FILMS_AND_SERIES_ABS_H_

#include <memory>
#include <string>

class FilmsAndSeriesAbs
{
	public:
		virtual void showAll() = 0;
		virtual void showSeries() = 0;
		virtual void showFilms() = 0;
		virtual void getInfo(const std::string &showName) = 0;
};

using FilmsAndSeriesAbsPtr =  std::shared_ptr<FilmsAndSeriesAbs>;

#endif /* _FILMS_AND_SERIES_ABS_H_ */
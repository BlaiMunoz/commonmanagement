#ifndef _DATA_BASE_MANAGER_H_
#define _DATA_BASE_MANAGER_H_

#include "dataBase/DataBaseManagerAbs.h"

#include <fstream>

class DataBaseManager: public DataBaseManagerAbs
{
	public:
        DataBaseManager();
		virtual ~DataBaseManager();
		bool loadDataBase(Json::Value &dataBase) override;

	private:
		std::ifstream mDBFile;
};

#endif /* _DATA_BASE_MANAGER_H_ */

#ifndef _DATA_BASE_MANAGER_ABS_H_
#define _DATA_BASE_MANAGER_ABS_H_

#include <memory>
#include "jsoncpp/json/json.h"

class DataBaseManagerAbs
{
	public:
		virtual bool loadDataBase(Json::Value &dataBase) = 0;
};

using DataBaseManagerAbsPtr =  std::shared_ptr<DataBaseManagerAbs>;

#endif /* _DATA_BASE_MANAGER_ABS_H_ */

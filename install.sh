# install packages
sudo apt-get install build-essential
sudo apt-get install cmake

# Build GTest suite
git clone https://github.com/google/googletest.git -b release-1.11.0
cd googletest        # Main directory of the cloned repository.
mkdir build          # Create a directory to hold the build output.
cd build
cmake ..             # Generate native build scripts for GoogleTest.
make all

cd ..
cp -r googlemock/include/* ../include/
cp -r googletest/include/* ../include/

# Install jsoncpp
$ sudo apt-get install libjsoncpp-dev